# openpgp_proof

[Verifying my OpenPGP key: openpgp4fpr:9AC21DA2D58590E94915C42513423DC3A607BB86]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs
